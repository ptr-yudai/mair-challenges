variables = {}

output = ""
with open("main.py", "r") as f:
    for line in f:
        line = line.rstrip('\n')
        for key in variables:
            if key in line:
                line = line.replace(key, variables[key])
        if ' = ' in line:
            x = line.split(" = ")
            name = x[0]
            value = ' = '.join(x[1:])
            if 'pnQmOE' in name:
                variables[name] = value
            else:
                if name != 'mkatz':
                    output += line + "\n"
        else:
            output += line + "\n"

with open("output.py", "w") as f:
    f.write(output)
